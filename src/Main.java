import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;

public class Main {

	Database db = new Database();
	Thread threadDb = new Thread(db);
	
	private JFrame frame;
	private JButton btnQuery;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		btnQuery = new JButton("Query");
		btnQuery.addActionListener(new BtnQueryActionListener() {
		});
		frame.getContentPane().add(btnQuery, BorderLayout.SOUTH);
		
		JTextPane textPane = new JTextPane();
		frame.getContentPane().add(textPane, BorderLayout.CENTER);
		
		JLabel lblQueryWindow = new JLabel("Query Window");
		frame.getContentPane().add(lblQueryWindow, BorderLayout.NORTH);
		
	
		}
	private class BtnQueryActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			//release the fox
			if (!threadDb.isAlive()) {
				threadDb = new Thread(db);
				threadDb.start();
			
			//release the hound
			Hound myHound = new Hound();
			Thread threadHound = new Thread(myHound);
			threadHound.start();

			} else {
				JOptionPane.showMessageDialog(null, "Hold your horses.");
			}
		}
	}
	private class Hound implements Runnable {

		@Override
		public void run() {
			try {
				threadDb.join();
				JOptionPane.showMessageDialog(null, db.getRunResult());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
	}
	

}
